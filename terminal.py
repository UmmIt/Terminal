import os
import signal
import time
import json

# ANSI color escape codes
class colors:
    GREEN = '\033[92m'
    YELLOW = '\033[93m'
    RED = '\033[91m'
    DARK_RED = '\033[31m'
    END = '\033[0m'

# ASCII banner
banner = """
    // | |                              //   ) )              //   ) )   
   //__| |            __  ___  ___     //        ( ) __  ___ //___/ /          // //  
  / ___  |   //   / /  / /   //   ) ) //  ____  / /   / /   / ____ / //   / / // //   
 //    | |  //   / /  / /   //   / / //    / / / /   / /   //       //   / / // //    
//     | | ((___( (  / /   ((___/ / ((____/ / / /   / /   //       ((___( ( // //
"""

creator = "Create by: UmmIt"
version = "v0.1"

def git_pull(path, url):
    try:
        os.chdir(path)  # Change directory to the repository path
        os.system("git pull origin pages")
    except Exception as e:
        print(f"{colors.RED}Error occurred while pulling updates from {url} in path {path}: {str(e).split(': ')[-1]}{colors.END}")

def list_commands():
    print(f"{colors.GREEN}Available commands:{colors.END}")
    print(f"{colors.YELLOW}  help       - List available commands{colors.END}")
    print(f"{colors.YELLOW}  pull       - Pull updates from Git repositories{colors.END}")
    print(f"{colors.YELLOW}  exit/quit  - Exit the program{colors.END}\n")

def main():
    # Print ASCII banner
    print(colors.DARK_RED + banner + colors.END)

    # Print creator and version in different colors
    print(f"{colors.YELLOW}{creator:<50}{colors.GREEN}{version:>30}{colors.END}\n")

    # List available commands
    list_commands()

    # Read paths and URLs from JSON file
    try:
        with open("paths_and_urls.json") as file:
            data = json.load(file)
            repositories = data["repositories"]
    except FileNotFoundError:
        print(f"{colors.RED}Error: File 'paths_and_urls.json' not found.{colors.END}")
        return

    # Signal handler for Ctrl+C
    def signal_handler(sig, frame):
        print(f"\n{colors.YELLOW}Exiting the program...{colors.END}")
        exit(0)
    signal.signal(signal.SIGINT, signal_handler)

    while True:
        command = input("> ").strip().lower()
        if command == "help":
            list_commands()
        elif command == "pull":
            for repo in repositories:
                try:
                    path = repo["path"]
                    url = repo["url"]
                    print(f"{colors.GREEN}Pulling updates from {url}...{colors.END}")
                    git_pull(path, url)
                    print(f"{colors.GREEN}Git pull executed for {url} in path {path}.{colors.END}")
                except KeyError:
                    print(f"{colors.RED}Error: Malformed JSON data. 'path' or 'url' key not found in repository.{colors.END}")
                except Exception as e:
                    print(f"{colors.RED}Unexpected error: {str(e).split(': ')[-1]}{colors.END}")
        elif command in ["exit", "quit"]:
            print(f"{colors.YELLOW}Exiting the program...{colors.END}")
            exit(0)
        else:
            print(f"{colors.RED}Unknown command. Type 'help' to list available commands.{colors.END}")

if __name__ == "__main__":
    main()

